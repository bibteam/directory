<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <a href="<?php echo site_url("auth/logout"); ?>">Logout</a>

        <br/>

        <!-- Add Phone Form -->
        <h4>Add Phone:</h4>
        <?php echo form_open("mydirectory/add_phone"); ?>
        <?php print_r(validation_errors()); ?>
        <label for="name">Name: </label>
        <input type="text" id="name" value="<?php echo set_value('name'); ?>" name="name"/>
        <label for="phone">Phone Number:</label>
        <input type="text" id="phone" value="<?php echo set_value('phone'); ?>" name="phone"/>
        <input type="submit" value="Add"/>
        <?php echo form_close(); ?>



        <br/>

        <table style="width:100%" border="1">
            <tr>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Delete</th>
            </tr>

            <?php foreach ($directory as $listing): ?>

                <tr>
                    <td><center><?php echo $listing['name']; ?></center></td>
        <td><center><?php echo $listing['phone_number']; ?></center></td>
        <td><center><a href="<?php echo site_url('mydirectory/delete/' . $listing["id"]); ?>">X</a></center></td>
    </tr>

<?php endforeach; ?>

</table>

<br/>
<?php echo $this->session->flashdata('delete_error'); ?>




</body>
</html>