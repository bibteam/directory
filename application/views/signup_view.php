
<html>

    <header>
        <title>
            <?php echo $title; ?>
        </title>
    </header>

    <?php echo form_open("auth/signup"); ?>

    <?php print_r(validation_errors()); ?>
    <label for="username">Username</label>
    <input type="text" id="username" name="username" value="<?php echo set_value('username'); ?>"/>

    <br/>
    <label for="password">Password</label>
    <input type="password" id="password" name="password"/>
    <br/>
    <label for="confirm_password">Confirm Password</label>
    <input type="password" id="confirm_password" name="confirm_password"/>
    <br/>
    <input type="submit" value="Sign up"/>

    <?php echo form_close(); ?>
    
    <br/>
    
    <a href="<?php echo site_url("auth/signin"); ?>">Login</a>



</html>
