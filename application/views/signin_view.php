
<html>

    <header>
        <title>
            <?php echo $title; ?>
        </title>
    </header>

    <?php echo form_open("auth/signin"); ?>

    <?php print_r(validation_errors()); ?>
    <?php if (isset($error_msg)) {
        echo $error_msg . "<br/>";
    } ?>
    <label for="username">Username</label>
    <input type="text" id="username" name="username" value="<?php echo set_value('username'); ?>"/>

    <br/>
    <label for="password">Password</label>
    <input type="password" id="password" name="password"/>
    <br/>
    <input type="submit" value="Login"/>

<?php echo form_close(); ?>
    
    <br/>
    
    <a href="<?php echo site_url("auth/signup"); ?>">Sign Up</a>



</html>
