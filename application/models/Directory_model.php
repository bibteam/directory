<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_model
 *
 * @author sharara
 */
class Directory_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert($phone, $name, $user_id) {

        $this->db->insert('phone', array("phone_number" => $phone, "name" => $name, "user_id" => $user_id));
        return $this->db->insert_id();
    }

    public function delete($id) {
        return $this->db->delete('phone', array('id' => $id));
    }
    
    public function get($id) {
        
        $query = $this->db->get_where('phone', array('id' => $id));
        
        return $query->row_array();
    }
    
    public function get_all($user_id) {
        $query = $this->db->get_where('phone', array('user_id' => $user_id));
        
        return $query->result_array();
    }

}
