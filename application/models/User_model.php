<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_model
 *
 * @author sharara
 */
class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function insert($username, $password) {
        
        $this->db->insert('user', array("username" => $username, "password" => md5($password) ));
        return $this->db->insert_id();
    }
    
    public function check_cred($username, $password) {
        $data = $this->db->get_where('user', array("username" => $username, "password" => md5($password) ));
        if($data->num_rows() == 1) {
            return $data->row_array();
        } else {
            return false;
        }
    }
    
    public function user_is_unique($username) {
        $data = $this->db->get_where('user', array("username" => $username));
        if($data->num_rows() == 1) {
            return false;
        } else {
            return true;
        }
    }
    
    
}
