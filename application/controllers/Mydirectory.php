<?php

/**
 * Description of Directory
 *
 * @author sharara
 */
class Mydirectory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("directory_model");
        $this->load->helper('form');
    }

    public function index() {

        
        if (!$this->session->userdata("id"))
            redirect("auth/signin");
        
        
        
        $data['directory'] = $this->directory_model->get_all($this->session->userdata("id"));
        
        $data["title"] = "Phone Directory";
        $this->load->view('directory_view', $data);
    }
    
    public function add_phone() {
        if (!$this->session->userdata("id"))
            redirect("auth/signin");

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[45]|alpha');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[7]|max_length[10]|numeric');

        if ($this->form_validation->run() == FALSE) {
            $data["title"] = "Phone Directory";
            $data['directory'] = $this->directory_model->get_all($this->session->userdata("id"));
            $this->load->view("directory_view", $data);
        } else {
            $name = $this->input->post("name");
            $phone = $this->input->post("phone");
            $id = $this->directory_model->insert($name, $phone, $this->session->userdata("id"));
            redirect("mydirectory");
        }
    }
    
    public function delete($id) {
        if (!$this->session->userdata("id"))
            redirect("auth/signin");
        
        $listing = $this->directory_model->get($id);
        
        if($listing["user_id"] != $this->session->userdata("id")) {
            $this->session->set_flashdata('delete_error', "You don't have permission to delete this entry");
        } else {
            $this->directory_model->delete($id);
        }
        
        redirect("mydirectory");
    }

}
