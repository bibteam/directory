<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author sharara
 */
class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("user_model");
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        
    }

    public function signup() {

        if ($this->session->userdata("id"))
            redirect("mydirectory");

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_is_unique');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data["title"] = "Sign Up";
            $this->load->view("signup_view", $data);
        } else {
            $username = $this->input->post("username");
            $password = $this->input->post("password");
            $id = $this->user_model->insert($username, $password);
            $this->session->set_userdata("id", $id);
            $this->session->set_userdata("username", $username);
            redirect("mydirectory");
        }
    }

    public function signin() {

        if ($this->session->userdata("id"))
            redirect("mydirectory");

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $data["title"] = "Login";
        if ($this->form_validation->run() == FALSE) {
            $this->load->view("signin_view", $data);
        } else {
            $username = $this->input->post("username");
            $password = $this->input->post("password");
            $user = $this->user_model->check_cred($username, $password);
            if ($user != FALSE) {
                $this->session->set_userdata("id", $user['id']);
                $this->session->set_userdata("username", $user['username']);
                redirect("mydirectory");
            } else {
                $data["error_msg"] = "Wrong username or password!";
                $this->load->view("signin_view", $data);
            }
        }
    }

    public function is_unique($username) {
        return $this->user_model->user_is_unique($username);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("mydirectory");
    }

}
