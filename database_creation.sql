CREATE SCHEMA IF NOT EXISTS `directory` DEFAULT CHARACTER SET utf8 ;
USE `directory` ;

-- -----------------------------------------------------
-- Table `directory`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `directory`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL,
  `password` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `directory`.`phone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `directory`.`phone` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `phone_number` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_phone_1_idx` (`user_id` ASC),
  CONSTRAINT `fk_phone_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `directory`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;
